<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetHousesController extends Controller
{
    public  function show(Request $request)
    {

        $house = DB::table('houses')
            ->where('name', '=', $request->name)
            ->orWhere(function ($query) use ($request){
                $query->whereBetween('price', [$request->pricestart, $request->pricend]);
            })
            ->orWhere(function ($query) use ($request){
                $query->where('bedrooms', '=', $request->bedrooms)
                    ->orWhere('bathrooms', '=', $request->bathrooms)
                    ->orWhere('storeys', '=', $request->storeys)
                    ->orWhere('garages', '=', $request->garages);
                    })
            ->get();

        return $house;

    }
}
