/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

//const SearchComponent =  window.Vue.component('search-component', require('./components/SearchForm.vue').default);
const ResultComponent =  window.Vue.component('result-component', require('./components/Result.vue').default);

const app = new Vue({
    el: '#app',
});


